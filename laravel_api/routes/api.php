<?php

Route::namespace('Auth')->group(function(){
    Route::post('register','RegisterController');
    Route::post('login','LoginController');
    Route::post('logout','LogoutController');
});

Route::get('user','UserController');

Route::namespace('Book')->middleware('auth:api')->group(function(){
    Route::post('add-book','BookController@store');
    Route::patch('update-book/{book}','BookController@update');
    Route::delete('delete-book/{book}','BookController@destroy');

    Route::post('pinjam','BorrowController@store');
    Route::patch('update-pinjam/{borrow}','BorrowController@update');
    Route::delete('delete-pinjam/{borrow}','BorrowController@destroy');

    Route::get('data-pinjam/{borrow}','Book\BorrowController@show');
});

Route::get('data-buku','Book\BookController@index');
// ngantuk pak middleware ga sempet :v
