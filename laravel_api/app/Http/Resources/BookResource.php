<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BookResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return[
        'Kode Buku'=> $this->kode,
        'Judul buku'=> $this->judul,
        'Pengarang'=> $this->pengarang,
        'Tahun Terbit'=> $this->tahun,
    ];
        // return parent::toArray($request);
    }
}
