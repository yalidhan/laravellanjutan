<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
class BorrowResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return[
            'Kode Pinjam'=> $this->kode_pinjam,
            'Tanggal Pinjam'=> $this->tgl_pinjam->format('d F Y'),
            'Batas Akhir'=> $this->batas_akhir->format('d F Y'),
            'Pengembalian'=> $this->pengembalian->format('d F Y'),
            'Pengembalian Tepat Waktu'=> $this->isOntime,
            'user'=>$this->user->name,
            'book'=>$this->book->judul,
        ];
        // return parent::toArray($request);
    }
}
