<?php

namespace App\Http\Controllers\Book;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Book\Book;
use App\Http\Resources\BookResource;
class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $books=Book::get();
        return BookResource::collection($books);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'kode_buku'=>['required'],
            'judul' =>['required'],
            'pengarang'=>['required'],
            'tahun'=>['required'],
        ]);
            // dd($request);
        $books=Book::create([
            'kode'=>request('kode_buku'),
            'judul'=>request('judul'),
            'pengarang'=>request('pengarang'),
            'tahun'=>request('tahun'),
        ]);
            // dd($borrows);
        return $books;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        Book::where('id',$id)
        ->update([
            'kode'=>request('kode_buku'),
            'judul'=>request('judul'),
            'pengarang'=>request('pengarang'),
            'tahun'=>request('tahun'),
        ]);
        // dd($update);
        $edited=Book::where('id',$id)->get();
        return $edited;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Book::where('id',$id)->delete();

        return response()->json('Data Buku Telah Dihapus');
    }
}
