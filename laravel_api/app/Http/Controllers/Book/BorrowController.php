<?php

namespace App\Http\Controllers\Book;
use App\Http\Controllers\Controller;
use App\Http\Resources\BorrowCollection;
use App\Http\Resources\BorrowResource;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\User;
use App\Models\Book\Borrow;
use App\Models\Book\Book;

class BorrowController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'kode_pinjam'=>['required'],
            'book_id' =>['required'],
            'batas_akhir'=>['required'],
            'pengembalian'=>['required'],
        ]);
            // dd($request);
        $borrows=auth()->user()->borrows()->create([
            'tgl_pinjam'=>Carbon::now(),
            'kode_pinjam'=>request('kode_pinjam'),
            'book_id'=>request('book_id'),
            'batas_akhir'=>request('batas_akhir'),
            'pengembalian'=>request('pengembalian'),
        ]);
            // dd($borrows);
        return $borrows;

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Borrow $borrow)
    {
        $borrows=Borrow::where('kode_pinjam',$borrow->kode_pinjam)->get();
        // dd($borrows);
        // return $borrows;
        return BorrowResource::collection($borrows);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Borrow $borrow)
    {
        //
        Borrow::where('kode_pinjam',$borrow->kode_pinjam)
        ->update([
            'pengembalian'=>request('pengembalian'),
            'isOntime'=>request('isOntime'),
        ]);
        $borrows=Borrow::where('kode_pinjam',$borrow->kode_pinjam)->get();
        // dd($borrows);
        // return $borrows;
        return BorrowResource::collection($borrows);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Borrow $borrow)
    {
        //

        Borrow::where('kode_pinjam',$borrow->kode_pinjam)->delete();

        return response()->json('Data Pinjam Buku Telah Dihapus');
    }
}
