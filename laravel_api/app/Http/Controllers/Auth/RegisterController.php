<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use App\Http\Requests\Auth\RegisterRequest;

class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(RegisterRequest $request)
    {
        //
        user::create([
            'name'=>request('name'),
            'jurusan'=>request('jurusan'),
            'fakultas'=>request('fakultas'),
            'hp'=>request('hp'),
            'wa'=>request('wa'),
            'email'=>request('email'),
            'password'=>bcrypt(request('password')),
            'nim'=>request('nim'),
        ]);

            return response('Terimakasih Sudah Mendaftar');
    }
}
