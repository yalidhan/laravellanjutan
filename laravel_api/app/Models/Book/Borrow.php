<?php

namespace App\Models\Book;

use Illuminate\Database\Eloquent\Model;
use App\Models\Book\Book;
use App\User;
class Borrow extends Model
{
    //
    protected $fillable=['tgl_pinjam','kode_pinjam','book_id','batas_akhir','pengembalian','isOnTime'];
    protected $dates = ['tgl_pinjam','batas_akhir','pengembalian'];
    protected $with =['user','book'];
    public  function getRouteKeyName()
    {
        return 'kode_pinjam';
    }
    
    public function user(){
        return $this->belongsTo(User::class);
    }

    public function book(){
        return $this->belongsTo(Book::class);
    }
}
