<?php

namespace App\Models\Book;

use Illuminate\Database\Eloquent\Model;
use App\Models\Book\Borrow;
class Book extends Model
{
    protected $fillable=['kode','judul','pengarang','tahun'];
    //
    public function borrows()
    {
        return $this->hasMany(Borrow::class);
    }
}
