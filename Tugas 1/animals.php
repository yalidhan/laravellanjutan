<?php 
    require_once 'utama.php';

    class Elang{
        use hewan, fight;

        public function __construct($nama){
            $this->nama=$nama;
            $this->keahlian="Terbang Tuinggi";
            $this->jumlahKaki=2;
            $this->attackPowa=10;
            $this->deffencePowa=5;
        }

        public function getInfoHewan(){
            $jenis=get_class($this);
            echo "<hr></br>Info Hewan: </br>";
            echo "Nama : ". $this->nama."</br>";
            echo "Jenis Hewan : ". $jenis."</br>";
            echo "Darah : ". $this->darah."</br>";
            echo "Jumlah Kaki : ". $this->jumlahKaki."</br>";
            echo "Keahlian : ". $this->keahlian."</br>";
            echo "Attack Power : ". $this->attackPowa."</br>";
            echo "Deffence Power : ". $this->deffencePowa."</br>";
        }
    }

    class Harimau{
        use hewan, fight;

        public function __construct($nama){
            $this->nama=$nama;
            $this->keahlian="Lari Cepat";
            $this->jumlahKaki=4;
            $this->attackPowa=7;
            $this->deffencePowa=8;
        }
        public function getInfoHewan(){
            $jenis=get_class($this);
            echo "<hr></br>Info Hewan: </br>";
            echo "Nama : ". $this->nama."</br>";
            echo "Jenis Hewan : ". $jenis."</br>";
            echo "Darah : ". $this->darah."</br>";
            echo "Jumlah Kaki : ". $this->jumlahKaki."</br>";
            echo "Keahlian : ". $this->keahlian."</br>";
            echo "Attack Power : ". $this->attackPowa."</br>";
            echo "Deffence Power : ". $this->deffencePowa."</br>";
        }
    }

?>