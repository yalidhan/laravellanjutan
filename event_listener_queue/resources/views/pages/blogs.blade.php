<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Create Blog Post</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.1/css/bootstrap.min.css" integrity="sha384-VCmXjywReHh4PwowAiWNagnWcLhlEJLA5buUprzK8rxFgeH0kww/aWY76TfkUoSX" crossorigin="anonymous">
</head>
<body>
    <div class="container">
        <div class="col-md-10">
            <h1>Blog Posts List</h1>
                <br>
            <center><a class="btn btn-success" href="/create" role="button">Post a Blog</a></center></br>
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
              @endif
              <br>
              <table class="table">
                <thead>
                    <tr>
                    <th scope="col">No</th>
                    <th scope="col">Judul Blog</th>
                    <th scope="col">Author</th>
                    <th scope="col">Dibuat Pada</th>
                    <th scope="col">Status Publikasi</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($blogs as $key => $blog)
                <tr>
                    <td>{{ $key +1}}</td>
                    <td>{{ $blog->judul}}</td>
                    <td>{{$blog->user->name}}</td>
                    <td>{{ $blog->created_at->format('d M Y')}}</td>
                    @if ($blog->publish_status==0)
                        <td align="center"><a class="btn btn-warning btn-sm" href="/blogs/{{$blog->slug}}" role="button" >In Review</a></td>
                        @elseif ($blog->publish_status==1)
                        <td align="center"><a class="btn btn-success btn-sm" href="/blogs/{{$blog->slug}}" role="button" >Published</a></td>
                    @endif
                    </tr>
                @endforeach
                </tbody>
            </table>

        </div>
    </div>

<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.1/js/bootstrap.min.js" integrity="sha384-XEerZL0cuoUbHE4nZReLT7nx9gQrQreJekYhJD9WNWhH8nEW+0c5qq7aIo2Wl30J" crossorigin="anonymous"></script>

</body>
</html>
