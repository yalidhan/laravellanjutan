<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Create Blog Post</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.1/css/bootstrap.min.css" integrity="sha384-VCmXjywReHh4PwowAiWNagnWcLhlEJLA5buUprzK8rxFgeH0kww/aWY76TfkUoSX" crossorigin="anonymous">
</head>
<body>
    <div class="container">
        <div class="col-md-10">
            <div class="card mt-3">
                <div class="card-header">
                    <small>By: {{$blog->user->name}} &nbsp Created at: {{ $blog->created_at->format('d M Y')}}</small>
                </div>
                <div class="card-body">
                  <h5 class="card-title">{{$blog->judul}}</h5>
                  <p class="card-text">{!!$blog->isi!!}</p>
                  @if ($blog->publish_status==0 && Auth::user()->is_editor==1)
                    <a href="/blogs/{{$blog->slug}}/accept" class="btn btn-success">Accept Blog Post</a>
                    @elseif($blog->publish_status==1)
                        <span class="badge badge-success">Post Accepted</span>
                        @else
                    <span class="badge badge-warning">Waiting For Editor Review</span>
                  @endif

                </div>
              </div>
              @if (session('status'))
              <div class="alert alert-success mt-5">
                  {{ session('status') }}
              </div>
            @endif
            <div class="mt-5 ">
                <a href="/blogs" class="btn btn-primary">Beranda</a>
            </div>
        </div>
    </div>

<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.1/js/bootstrap.min.js" integrity="sha384-XEerZL0cuoUbHE4nZReLT7nx9gQrQreJekYhJD9WNWhH8nEW+0c5qq7aIo2Wl30J" crossorigin="anonymous"></script>

</body>
</html>
