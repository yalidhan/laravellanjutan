<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::group(['middleware'=>'auth'],function(){
    Route::get('/create','BlogController@create');
    Route::post('/store','BlogController@store');
    Route::get('/blogs','BlogController@index');
    Route::get('/blogs/{blog}','BlogController@show');
    Route::get('/blogs/{blog}/accept','BlogController@accept');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
