<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    //
    protected $fillable=['id_user','judul','isi','slug'];

    public function getRouteKeyName()
{
    return 'slug';
}
    public function user(){
        return $this->belongsTo(User::class,'id_user');
    }
}
