<?php

namespace App\Listeners;

use App\Events\BlogPublishStatusEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Blog;
use App\User;
use App\Mail\BlogPublishStatusMail;
use Mail;
use Illuminate\Support\Facades\Auth;

class SendEmailNotificationBlogPost implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  BlogPublishStatusEvent  $event
     * @return void
     */
    public function handle(BlogPublishStatusEvent $event)
    {
        $useremail=User::select('email')->where('id',$event->blog->id_user)->first();
        Mail::to($useremail)->send(new BlogPublishStatusMail($event->blog));
    }

}
