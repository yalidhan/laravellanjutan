<?php

namespace App\Listeners;

use App\Events\BlogPublishStatusEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Blog;
use App\User;
use App\Mail\EditorNotifMail;
use Mail;
class SendEmailNotificationBlogEditor implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  BlogPublishStatusEvent  $event
     * @return void
     */
    public function handle(BlogPublishStatusEvent $event)
    {
        //
        $reviewermail=User::where('is_editor',1)->get();
        Mail::to($reviewermail)->send(new EditorNotifMail($event->blog));
    }
}
