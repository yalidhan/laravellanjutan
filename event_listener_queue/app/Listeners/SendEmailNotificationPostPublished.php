<?php

namespace App\Listeners;

use App\Events\BlogPublishedPostEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\User;
use App\Blog;
use App\Mail\BlogPublishedMail;
use Mail;
class SendEmailNotificationPostPublished implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  BlogPublishedPostEvent  $event
     * @return void
     */
    public function handle(BlogPublishedPostEvent $event)
    {
        //
        $useremail=User::select('email')->where('id',$event->blog->id_user)->first();
        // dd($blog);
        // $blog=Blog::where('id',$blog->id)->get();
        Mail::to($useremail)->send(new BlogPublishedMail($event->blog));
    }
}
