<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Blog;
use App\User;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use App\Events\BlogPublishStatusEvent;
use App\Events\BlogPublishedPostEvent;
use App\Mail\BlogPublishedMail;
use Mail;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $blogs=Blog::get();
        return view('pages.blogs',compact('blogs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view("pages.create_blog");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $blog=Blog::create([
            'judul'=>$request->judul,
            'isi'=>$request->isi,
            'slug'=>Str::slug($request->judul,'-'),
            'id_user'=>$request->id_user
        ]);

        if ($blog){
            event(new BlogPublishStatusEvent($blog));
            return redirect('/blogs')->with('status','Blog Berhasil Ditambahkan!, Silahkan tunggu konfirmasi dari Editor');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Blog $blog)
    {
        //
        return view('pages.blog',compact('blog'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }
    public function accept(Blog $blog)
    {
        //
        DB::table('blogs')
            ->where('id', $blog->id)
            ->update(['publish_status'=>1]);
         event(new BlogPublishedPostEvent($blog));
        return redirect('/blogs/'.$blog->slug)->with('status','Blog Has Been Accepted');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
