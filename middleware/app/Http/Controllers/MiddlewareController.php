<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class MiddlewareController extends Controller
{
    //
    public function admin(){
        return view('pages.admin');
    }

    public function guest(){
        return view('pages.guest');
    }
    public function superadmin(){
        return view('pages.superadmin');
    }
}
