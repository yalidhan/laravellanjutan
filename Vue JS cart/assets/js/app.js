var app = new Vue({
    el : '#app',
    data: {
        cart:{
            items:[]
        },
        products:[
            {
                id:1,
                name:"Star Wars Tumbler",
                description:
                "Tumbler for Star Wars series fans",
                imgSrc:"public/img/tumbler3.jpg",
                price:100000,
                inStock:5,
            },
            {
                id:2,
                name:"New B. Tumbler",
                description:
                "400Ml & 700Ml Capacity available",
                imgSrc:"public/img/tumbler1.jpg",
                price:70000,
                inStock:10,
            },
            {
                id:3,
                name:"Plain Tumbler",
                description:
                "Good for custom purpose",
                imgSrc:"public/img/tumbler2.jpg",
                price:50000,
                inStock:20,
            },
        ]

    },
    // computed properties
    computed : {
        cartTotal:function(){
            var total=0;
            this.cart.items.forEach(function(item){
                total +=item.quantity * item.product.price;
            });
            return total;
        }
    },
    // method properties
    methods: {
        addProductToCart: function(product){
            var cartItem=this.getCartItem(product);
            if(cartItem !=null){
                cartItem.quantity++;
            }  else{
                this.cart.items.push({
                    product:product,
                    quantity:1
                });
            }
            product.inStock--;
        },
        getCartItem:function(product){
            for (var i = 0; i< this.cart.items.length;i++){
                if(this.cart.items[i].product.id===product.id){
                    return this.cart.items[i];
                }
            }
        },
        increaseQuantity: function(cartItem){
            cartItem.product.inStock--;
            cartItem.quantity++;
        },
        decreaseQuantity: function(cartItem){
            cartItem.quantity--;
            cartItem.product.inStock++;
            if(cartItem.quantity==0){
                this.removeItemFromCart(cartItem);
            }
        },
        removeItemFromCart:function(cartItem){
            var index = this.cart.items.indexOf(cartItem);

            if(index !==-1){
                this.cart.items.splice(index,1)
            }
        },
        checkout : function(){
            if (confirm('Anda yakin ingin membeli produk-produk ini?')){
                this.cart.items.forEach(function(item){
                    item.product.inStock += item.quantity;
                });
                this.cart.items =[];
            }
        }

    },
    filters: {
        currency: function(value) {
          var formatter = new Intl.NumberFormat("id-ID", {
            style: "currency",
            currency: "IDR",
            minimumFractionDigits: 0
          });
          return formatter.format(value);
        }
      },
});



